We extracted spreadsheet tables (in Excel and HTML format) from [INSEE](https://www.insee.fr/fr/statistiques)
automatically and stored the extracted data in [RDF](https://www.w3.org/RDF/) format.
The RDF dataset is available as ``insee-rdf.ttl.gz`` in this repository.

We developed a system to extract this dataset because this dataset is very useful to fact-check claims but there's no
public API to give us its access. 
The details of the extraction process can be found in
[https://gitlab.inria.fr/cedar/excel-extractor](https://gitlab.inria.fr/cedar/excel-extractor) and
[https://gitlab.inria.fr/cedar/excel-search](https://gitlab.inria.fr/cedar/excel-search).
We also described the details in our paper [Extracting linked data from statistic spreadsheets
](https://dl.acm.org/citation.cfm?id=3066914).
